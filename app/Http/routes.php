<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
| 
*/

/*Email*/
Route::match(['get','post'],'/contact/mail', function (\Illuminate\Http\Request $request) {
    //test queues and workers are working
    $email ='contact@notifyme.ng';
    $email_details = array('email' =>$request->email ,'message'=>$request->message );
    //return $email;
    Mail::queue('email.hello',['email_details'=>$email_details],
        function ($message) use ($email)
        {
            $message->to($email, '')
                ->from('contact@notifyme.ng',$name = 'Notifyme')
                ->subject('Contact Notifyme');
        });
    echo 'Your message has been sent. We will Contact you shortly';
});

/*States*/
Route::get('states','LocationController@showStates');

Route::get('/', 'PagesController@HomePage');
Route::get('/about', 'PagesController@AboutPage');
Route::get('/features', 'PagesController@FeaturesPage');
Route::get('/contact', 'PagesController@ContactPage');

Route::auth();

Route::get('/home', 'HomeController@index');

/*Payment*/
    Route::get('/payment', 'PaymentController@payment');
    Route::post('/payment/callback', 'PaymentController@payment_callback');
    Route::get('/ref_code/{months}','PaymentController@ref_code');

/*Email*/
	Route::match(['get','post'],'email/save','EmailController@saveEmail');

/*Phone*/
	Route::match(['get','post'],'phone/save','PhoneController@savePhone');

/*Location*/
	Route::match(['get','post'],'location/save','LocationController@saveLocation');
	Route::match(['get','post'],'latlong/{state}/{city}/{address}','LocationController@getLongLat');

/*User to Phone model relationship*/ 
	//Route::get('/user','SubscriberController@UserDetails');	
	Route::post('/user/update','SubscriberController@UpdateDetails');
	Route::get('/auth','SubscriberController@authDetails');


/*Admin*/
Route::group(['middleware' => 'admin'], function () {
	Route::get('/admin','AdminController@usersList');
});

/*Template*/
Route::get('template',function(){
	return view('layouts.frontend');
});