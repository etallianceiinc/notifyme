<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Event;
use App\Events\UserRegistration;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Carbon\Carbon;
use Mail;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    protected function authenticated($request, $user){
        if($user->admin) {
            return redirect()->intended('/admin');
        }
        return redirect()->intended('/home');
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:40',
            'last_name' => 'required|max:40',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   
        $trial_day=Carbon::now()->addDays(30);
        //event(new UserRegistration($user));
        /*$mail=Mail::send('email.hello',['user' => 'hee'], function ($message){
            $message->from('noc@notifyme.ng', 'Notifyme.ng');
            $message->to('odusanyokunle@gmail.com');
            $message->subject('SoundClash Nigeria');
            });
       */
        $user= User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'end_of_subscription'=>$trial_day,
            'trial' => 1,
        ]);

        return $user;
    }
}
