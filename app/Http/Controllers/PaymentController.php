<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Payment;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\User;
use Carbon\Carbon;
class PaymentController extends Controller
{
    public function payment(request $request){
        $private_key=getenv("PAYSTACK_SECRET_KEY");
        $txn_ref=$request->reference;
        $uri="https://api.paystack.co/transaction/verify/{$txn_ref}";
        $response = \Httpful\Request::get($uri)->expectsJson()->addHeader('Authorization', "Bearer {$private_key}")->send();
        $response= json_decode((string) $response,true);
        if ($response['status']) {            
            //return $response['data'];
            $amount=$response['data']['amount'];
            $reference=$response['data']['reference'];
            $payment_confirmed= Payment::where('payment_reference',$reference)->first();
            

            //print_r($payment_confirmed);
            if ($payment_confirmed->amount==$amount) {
                $payment_confirmed->status='paid';
                $payment_confirmed->save();
                /*Update subsciption end date*/
                $user_id= $payment_confirmed->user_id;
                $update_subscription_date= User::where('id',$user_id)->first();
                $months_paid=($amount/50000);
                //return $months_paid;
                
                /*Get details of last payment expiration date*/                
                $now=$this->lastSubscriptionDate();
                date_add($now,date_interval_create_from_date_string( $months_paid.'months'));
                $end_date=date_format($now,"Y-m-d H:i:s");

                $update_subscription_date->end_of_subscription=$end_date;
                $update_subscription_date->trial=0;
                $update_subscription_date->subscriber=1;
                $update_subscription_date->save();
                return $update_subscription_date;
            }
            $post_payment= Payment::where('payment_reference',$reference)->first();
            return $post_payment;
        }


        return $request->all();
    }

    public function ref_code($months){
        $random= rand();
        $random=hash("sha256",$random);
        $id=Auth::user()->id;
        $amount=($months*50000);
        $save_request= Payment::create([
            'payment_reference' => $random,
            'amount' => $amount,
            'user_id'=>$id,
        ]);

        switch($months){
            case 1:
                $plan='PLN_z8ksv9fahr0yijd';
                break;
            case 12:
                $plan='PLN_6mqt2lc09fscip0';
                break;
            default:
                $plan=null;
        }
        $payment = array('ref' => $random,
            'id'=>$id,
            'months'=>$months,
            'email'=>Auth::user()->email,
            'amount'=>$amount,
            'key'=>getenv("PAYSTACK_PUBLIC_KEY"),
            'plan'=>$plan);
        return $payment;
    }

    public function payment_callback(request $request){
        return $request->all();
    }

    public function lastSubscriptionDate()
    {
        /*Today's date*/
        $now=date_create();

        /*Last Subscription Date*/
        $subscription=date_create(Auth::user()->end_of_subscription);
        
        /*Difference between dates*/
        $diff=date_diff($now,$subscription);
        $status=$diff->format("%R%a");
        if ($status>=0) {
            return $subscription;
            //return date_format($subscription,"Y/m/d H:i:s");
        }
        elseif ($status<0) {
            return $now;
            //return date_format($now,"Y/m/d H:i:s");
        }
    }
}
