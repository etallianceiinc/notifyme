<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;
use DB;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function usersList()
    {	$users=$this->allUsers();
    	//return count($users);
    	//return $users;
    	return view('layouts.export',['users'=>$users]);
    }
}
