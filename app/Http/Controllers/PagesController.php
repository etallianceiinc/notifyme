<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
    public function ContactPage(){
    	return view('pages.contact');
    }

    public function FeaturesPage(){
    	return view('pages.features');
    }

    public function AboutPage(){
    	return view('pages.about');
    }

    public function HomePage(){
    	//return view('pages.home');
        return view('canvas_home');
    }
}
