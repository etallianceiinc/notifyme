<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Phone;

class PhoneController extends Controller
{
    public function savePhone(request $request){
    	$user_id=Auth::user()->id;
    	$id=$request->id;
        $phone=$request->phone;
        $country_code=$request->country;
        $save=Phone::firstOrNew(['id'=>$id,
            'user_id'=>$user_id,
            ])->save(['id'=>$id,
                'user_id'=>$user_id,
                'phone'=>$phone,
                'phone_country' => $country_code,
            ]);
        $update=Phone::find($id)->where('user_id',$user_id)
        ->where('id',$id)
        ->update(['phone'=>$phone,'phone_country' => $country_code]);
        if ($update) {
            $msg='phone';
    	   return $this->Successresponse('phone');	                        
        }
    }
}
