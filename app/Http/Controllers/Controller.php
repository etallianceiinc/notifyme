<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use guzzle;
use App\User;
class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function Successresponse($value)
    {
    	$data = array('status' => 'ok',
    		'msg'=> $value.' successful'
    		 );
    	return $data;
    }

    static  function guzzle($url){
    	return new guzzle(['base_uri' => $url]);
    }

    public function findUser($id)
    {        
        $user = User::find($id)
        ->where('id',$id)
        ->with('locations', 'payments','emails','phones')->get()->toArray();
        return $user;
    }

    public function allUsers()
    {
        $users = User::with('locations', 'payments','emails','phones')->get();
        return $users;
    }
}
