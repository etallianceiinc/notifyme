<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Email;

class EmailController extends Controller{

    public function saveEmail(request $request){
    	$user_id=Auth::user()->id;
    	$id=$request->id;
        $email=$request->email;
    	//return $id;
        $save=Email::firstOrNew(['id'=>$id,
            'user_id'=>$user_id,
            ])->save(['id'=>$id,
                'user_id'=>$user_id,
                'email'=>$email,
            ]);
        $update=Email::find($id)->where('user_id',$user_id)
        ->where('id',$id)
        ->update(['email'=>$email]);
        if ($update) {
            $msg='email';
    	   return $this->Successresponse('email');	                        
        }
    }
}
