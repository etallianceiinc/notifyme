<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Auth;
class SubscriberController extends Controller
{
    public function UserDetails(){
        $id=Auth::user()->id;
        $user=$this->findUser($id);
        return $user[0];
    }
    public function authDetails($value=''){
    	$det=Auth::user()->all();
    	return $det;

    }

    public function UpdateDetails(request $request){
        $input = array('first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                 );
        $save= User::find(Auth::user()->id)->update($input);
        if ($save) {
            return redirect('home')->with('status','saved');
        }
        return redirect('home');
    }
}
