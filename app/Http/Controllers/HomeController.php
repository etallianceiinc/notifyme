<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //$now= Carbon::now(); 
        //$subscription=Carbon::create(Auth::user()->end_of_subscription);

        /*Get all country codes*/
        
        //$uri=asset("countries_data.json");
        $uri="https://restcountries.eu/rest/v1/all";
        //$uri= asset('countries_data.json');
        $response = \Httpful\Request::get($uri)->send();
        $country_codes= json_decode((string) $response,true);
        //return $response;

        $states=$this->getStates();
        //return $states;

        $now=date_create();
        $subscription=date_create(Auth::user()->end_of_subscription);
        $diff=date_diff($now,$subscription);
        $status=$diff->format("%R%a");
        //$status= $status;
        //$status='active';
        if ($status>=0) {
            $status='active';
        }        
        elseif ($status<0) {
            $status='expired';
        }
        $id=Auth::user()->id;
        $user=$this->findUser($id);
        //return $user[0];
        //return $status;
        $timestamp = strtotime(Auth::user()->end_of_subscription);
        $locations = array('0' => 'Home', 
                            '1' => 'Office',
                            '2'=> 'Other 1',
                            '3' => 'Other 2',
                            '4' => 'Other 3'
            );
        return view('home',['subscription'=>$timestamp,'status'=>$status,'country_codes'=>$country_codes, 'states'=>$states, 'location_type'=>$locations, 'user' => $user[0]]);
    }

    private function getStates()
    {
        /*Get all states*/
        $uri=asset('states_data.json');
        $response = \Httpful\Request::get($uri)->send();
        $states= json_decode((string) $response,true);
        return $states;
    }
}
