<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Location;
use App\State;

class LocationController extends Controller
{
	protected $key="AIzaSyAEXKfbtxOEECFufrNx8oOtGAoe_crGWjg";
	protected $uri='https://maps.googleapis.com/maps/api/geocode/json?';
	protected $google='https://maps.googleapis.com/maps/api/geocode/json?address=prince ajiboye crescent&region=NG&key=AIzaSyAEXKfbtxOEECFufrNx8oOtGAoe_crGWjg';

    public function saveLocation(request $request){
    	//return $request->all();
    	$state=$request->state;
    	$address=$request->street_address;
    	$city=$request->city;
    	//return $address;
    	$id=$request->id;
    	$user_id=Auth()->user()->id;
    	$latlng=$this->getLongLat($address,$state,$city);
    	//return $latlng;
    	$location = array('id' => $id,
    						'user_id'=> $user_id,
    						'location'=>$request->location,
    						'street_address'=>$request->street_address,
    						'city'=>$request->city,
    						'state'=>$request->state,
    						'longitude'=>$latlng['lng'],
    						'latitude'=>$latlng['lat'],
    	 );
    	//return $location;
    	$save=Location::firstOrNew(['id'=>$id,
            'user_id'=>$user_id,
            ])->save($location);
        $update=Location::find($id)->where('user_id',$user_id)
        ->where('id',$id)
        ->update($location);
        if ($update) {
            $msg='location';
    	   return $this->Successresponse('location');	                        
        }

    }

    public function showStates(){
    	$states=State::all();
    	return $states;
    }

    public function getLongLat($address='Lagos',$state='Lagos',$city='ikeja'){	
    	/*Check for address on google maps*/ 
    	$query='region=NG'.'&address='.$address.','.$city.'&key='.$this->key;
    	//return $query;
    	$visit = $this->uri.$query;
    	//return $visit;
    	//$maps=file_get_contents($visit);
    	//return $maps;
        //return $response;
    	$response = \Httpful\Request::get($visit)->send();
        $response= json_decode((string) $response,true);
        if ($response['status']=='OK') {
    	$latlng = array('lat' => $response['results'][0]['geometry']['location']['lat'], 
    					'lng'=>$response['results'][0]['geometry']['location']['lng']);
    	return $latlng;
        }
        //return $response['results'][0]['geometry']['location'];
        /*If failed, get state and use saved latlng value*/
        elseif ($response['status']!='OK') {
        	$stateLatLng=State::where('name',$state)->first();
    		$latlng = array('lat' => $stateLatLng->latitude, 'lng'=>$stateLatLng->longitude);
			return $latlng;
        }
    	$latlng = array('lat' => 0.00000, 'lng'=>0.00000);
    	return $latlng;
    }
}
