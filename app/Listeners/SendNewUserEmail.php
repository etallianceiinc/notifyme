<?php

namespace App\Listeners;

use App\Events\UserRegistration;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendNewUserEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistration  $event
     * @return void
     */
    public function handle(UserRegistration $event)
    {
        //
    }

    public function sendEmail()
    {
        Mail::send('how we roll', function ($message){
            $message->from('info@soundclashnigeria.com', 'SoundClash Nigeria');
            $message->to('odusanyokunle@gmail.com');
            $message->subject('SoundClash Nigeria');
            });
    }
}
