<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','phone','end_of_subscription','trial', 'subscriber'
    ];

     /* The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function emails()
    {
        return $this->hasMany('App\Email');
    }

    public function locations()
    {
        return $this->hasMany('App\Location');
    }

    public function phones()
    {
        return $this->hasMany('App\Phone');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function updateDetails($value='')
    {
        # code...
    }

    public function allUsers($value='')
    {
        
    }
}
