<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'location', 'user_id','id','street_address','lga','city','state','country','longitude','latitude',
    ];
}
