@extends('layouts.notifyme')

@section('content')
 <!--slider-bg starts-->
<div id="slider-bg">
    <div class="container">
      <div class="row content-top"> 
        
        <!--slides starts-->
        <div class="span12">
          <div id="slider" class="flexslider">
            <ul class="slides">
              
              <!--slide one starts-->
              <li>
                <div class="row">
                  <div class="span6">
                    <h2>Promise to create <br/>
                      unique experiences for web and mobile</h2>
                    <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum qui blandit praesent feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </p>
                  </div>
                  <div class="span6"> <img src="img/preview/tab-slide1.png" alt="slide"/> </div>
                </div>
              </li>
              <!--slide one ends--> 
              
              <!--slide two starts-->
              <li>
                <div class="row">
                  <div class="span6">
                    <h2>Responsive interface <br/>
                      became necessity for various devices</h2>
                    <p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui zzril delenit quam littera qui blandit praesent augue duis dolore te feugait nulla facilisi. </p>
                  </div>
                  <div class="span6"> <img src="img/preview/tab-slide2.png" alt="slide"/> </div>
                </div>
              </li>
              <!--slide two ends--> 
              
              <!--slide three starts-->
              <li>
                <div class="row">
                  <div class="span6">
                    <h2>Easy to edit & <br/>
                      customize templates for web and mobile</h2>
                    <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril in iis qui facit eorum delenit augue duis dolore te feugait nulla facilisi. </p>
                  </div>
                  <div class="span6"> <img src="img/preview/tab-slide3.png" alt="slide"/> </div>
                </div>
              </li>
              <!--slide three ends--> 
              
              <!--slide four starts-->
              <li>
                <div class="row">
                  <div class="span6">
                    <h2>Awesome creativity<br/>
                      & passion to develop apps for mobile devices</h2>
                    <p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui qui blandit praesent zzril gothica delenit augue duis dolore te feugait nulla facilisi. </p>
                  </div>
                  <div class="span6"> <img src="img/preview/tab-slide4.png" alt="slide"/> </div>
                </div>
              </li>
              <!--slide four ends--> 
              
              <!--slide five starts-->
              <li>
                <div class="row">
                  <div class="span6">
                    <h2>Our cloud thinking <br/>
                      process creates unlimited possibilities</h2>
                    <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te qui blandit praesent feugait nulla facilisi. </p>
                  </div>
                  <div class="span6"> <img src="img/preview/tab-slide5.png" alt="slide"/> </div>
                </div>
              </li>
              <!--slide five ends-->
              
            </ul>
          </div>
        </div>
        
        <!--slides ends--> 
        
        <!--thumbnails starts-->
        <div class="span12">
          <div id="thumb-slider" class="flexslider">
            <ul class="slides">
              <li>
                <div class="thumb-text clearfix"><img src="img/icons/slider-tab-icon1.png" width="64" height="64" alt="icon">
                  <p>Awesome web templates</p>
                </div>
              </li>
              <li>
                <div class="thumb-text clearfix"><img src="img/icons/slider-tab-icon2.png" width="64" height="64" alt="icon">
                  <p>Responsive layout</p>
                </div>
              </li>
              <li>
                <div class="thumb-text clearfix"><img src="img/icons/slider-tab-icon3.png" width="64" height="64" alt="icon">
                  <p>Easy to customize </p>
                </div>
              </li>
              <li>
                <div class="thumb-text clearfix"><img src="img/icons/slider-tab-icon4.png" width="64" height="64" alt="icon">
                  <p>State of art design</p>
                </div>
              </li>
              <li>
                <div class="thumb-text clearfix"><img src="img/icons/slider-tab-icon5.png" width="64" height="64" alt="icon">
                  <p>Infinite modules</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <!--thumbnails ends--> 
        
      </div>
    </div>
</div>
  <!--slider-bg ends-->
  
<div class="container"> 
    
    <!--features starts-->
    <div class="row">
      <div class="span12">
        <h1>Unique experiences for <span class="color">web</span> & <span class="color">mobile</span></h1>
        <ul class="list-3col text-center">
          <li>
            <div class="icon-bg"><img src="img/icons/icon1-iphone.png" width="120" height="120" alt="icon"></div>
            <h2>Responsive</h2>
            Duis autem vel eum iriure qui  dolor est etiam in velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros. </li>
          <li>
            <div class="icon-bg"><img src="img/icons/icon1-leaf.png" width="120" height="120" alt="icon"></div>
            <h2>Customizable</h2>
            Autem vel eum praesent dolor in hendrerit in vulputate velit esse molestie consequat, at vero vel blandit illum dolore eu feugiat nulla.</li>
          <li>
            <div class="icon-bg"><img src="img/icons/icon1-create.png" width="120" height="120" alt="icon"></div>
            <h2>Documented</h2>
            Eodem modo typi qui nunc nobis videntur parum clari, fiant sollemnes  est ad minim usus  liber tempor option legentis in futurum.</li>
        </ul>
      </div>
    </div>
    <!--features ends--> 
    
    <!--spacer here-->
    <div class="spacer-40px"></div>
    
    <!--carousel starts-->
    <div class="row">
      <div class="span12"> 
        
        <!--icon and heading-->
        
        <h4 class="heading-icon clearfix"><img src="img/icons/heading-icon-1.png" width="40" height="40" alt="icon" class="icon-small-bg"> Features at a glance</h4>
        
        <!--carousel list starts-->
        <div id="carousel" class="es-carousel-wrapper">
          <div class="es-carousel">
            <ul>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-1.jpg" alt="image" /> <a href="img/preview/thumb-1.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>Easy to use lorem interface</h5>
                <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum assum dolore eu feugiat nulla.</p>
              </li>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-2.jpg" alt="image" /> <a href="img/preview/thumb-2.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>Cost effective ipsum amet</h5>
                <p>Typi non habent vero claritatem insitam usus legentis in iis qui facit eorum volutpat est legere lius quod ullamcorper legentis legunt. </p>
              </li>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-3.jpg" alt="image" /> <a href="img/preview/thumb-3.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>Clean layout for lorem</h5>
                <p>Duis autem vel eum iriure dolor in congue velit hendrerit in vulputate velit esse dolore eu feugiat nulla facilisis at vero  congue nihil.</p>
              </li>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-4.jpg" alt="image" /> <a href="img/preview/thumb-4.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>24x7 support on sit amet</h5>
                <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum assum dolore eu feugiat nulla.</p>
              </li>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-5.jpg" alt="image" /> <a href="img/preview/thumb-5.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>Instant updation sit</h5>
                <p>Typi non habent vero claritatem insitam usus legentis in iis qui facit eorum volutpat est legere lius quod ullamcorper legentis legunt. </p>
              </li>
              <li>
                <div class="folio-thumb"><img src="img/preview/thumb-6.jpg" alt="image" /> <a href="img/preview/thumb-6.jpg" data-rel="prettyPhoto[gallery1]" title="This is title of Image" class="icon-zoom"></a></div>
                <h5>Visually appealing dolor</h5>
                <p>Duis autem vel eum iriure dolor in congue velit hendrerit in vulputate velit esse dolore eu feugiat nulla facilisis at vero  congue nihil.</p>
              </li>
            </ul>
          </div>
        </div>
        <!--carousel list ends--> 
        
      </div>
    </div>
    <!--carousel ends--> 
    
    <!--spacer here-->
    <div class="spacer-40px"></div>
    
    <!--row starts-->
    <div class="row"> 
      
      <!--column one starts-->
      <div class="span6">
        <h4 class="heading-icon clearfix"><img src="img/icons/heading-icon-2.png" width="40" height="40" alt="icon" class="icon-small-bg"> Very powerful options built right in</h4>
        <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel sequitur mutationem illum dolore eu feugiat nulla.</p>
        
        <!--two column list (nested columns) starts-->
        <div class="row">
          <ul class="span3 list-checkmark">
            <li>Easy to use lorem interface</li>
            <li>Cost effective ipsum amet </li>
            <li>Clean layout for lorem</li>
          </ul>
          <ul class="span3 list-checkmark">
            <li>24x7 support on sit amet</li>
            <li>Instant updation sit</li>
            <li>Visually appealing dolor</li>
          </ul>
        </div>
        <!--two column list starts--> 
        
        <a href="#">Learn more &raquo;</a> </div>
      <!--column one ends--> 
      
      <!--column two starts-->
      <div class="span6"> <img src="img/preview/img-window.png" alt="image"> </div>
      <!--column two ends--> 
      
    </div>
    <!--row ends--> 
    
    <!--spacer here-->
    <div class="spacer-30px"></div>
    
    <!--box starts-->
    <div class="row">
      <div class="span12">
        <div class="dark-box">
          <h1><span class="color">Get started</span> today with a free trial!</h1>
          <ul class="list-separator">
            <li>14 days free trial <span class="separator">|</span></li>
            <li> No credit card required <span class="separator">|</span> </li>
            <li> <span class="color">$10/mo</span> after end of trial period</li>
          </ul>
          <a href="#" class="mybtn">Download</a> </div>
      </div>
    </div>
    <!--box ends--> 
    
</div>
@endsection
