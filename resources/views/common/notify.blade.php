@if (session('status'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>Well done!</strong> You successfully updated your profile.
</div>
@endif