<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta name="description" content="critical event communication" />
<meta name="keywords" content=" road accidents, crime, fire, flooding, emergency, riots, burglary, infectious disease outbreak"/>
<meta name="author" content="kunleodusan" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<title>Notifyme</title>

<!--Fav and touch icons-->
<link rel="shortcut icon" href="{{asset('img/favico.png')}}">
<link rel="apple-touch-icon" href="{{asset('img/icons/apple-touch-icon.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-touch-icon-114x114.png')}}">

<!--google web font-->
<link href='http://fonts.googleapis.com/css?family=Raleway:400,600,700' rel='stylesheet' type='text/css'>

<!--style sheet-->
<link rel="stylesheet" media="screen" href="{{asset('css/bootstrap.css')}}"/>
<link rel="stylesheet" media="screen" href="{{asset('css/bootstrap-responsive.css')}}"/>
<link rel="stylesheet" media="screen" href="{{asset('css/style.css')}}"/>
<!-- Custom CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!--jquery libraries / others are at the bottom-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="{{asset('js/modernizr.js')}}" type="text/javascript"></script>

<!--elastislide carousel script starts-->
<link rel="stylesheet" media="screen" href="{{asset('css/elastislide.css')}}"/>
<script type="text/javascript" src="{{asset('js/jquery.elastislide.js')}}"></script>

<script type="text/javascript">
  $(document).ready(function() {
  $('#carousel').elastislide({
  		imageW 		: 300,
  		margin		: 20,
  		border		: 0,
  		easing		: 'easeInBack'
  });
  });
</script>
<!--elastislide carousel script ends-->

<!--prettyphoto scripts starts-->
<link rel="stylesheet" media="screen" href="{{asset('css/prettyPhoto.css')}}"/>
<script src="{{asset('js/jquery.prettyPhoto.js')}}" type="text/javascript"></script>

<!--prettyphoto scripts ends-->

<!--flexslider scripts starts-->
<link rel="stylesheet" media="screen" href="{{asset('css/flexslider.css')}}"/>
<script src="{{asset('js/jquery.flexslider-min.js')}}" type="text/javascript"></script>
<script type="text/javascript"> 
  // Slider with thumbnail
  $(document).ready(function() {
  		$('#thumb-slider').flexslider({
  		animation: "slide",
  		controlNav: false,
  		animationLoop: false,
  		slideshow: true,
  		directionNav: false,
  		controlNav: false,
  		itemWidth: 180,
  		itemMargin: 0,
  		asNavFor: '#slider'
  		});
  		$('#slider').flexslider({
  		animation: "slide",
  		controlNav: false,
  		directionNav: false,
  		animationLoop: true,
  		slideshow: true,
  		sync: "#thumb-slider"
          });
  });
</script>
<!--flexslider scripts ends-->

</head>
<body>

<!-- header starts
================================================== -->
<section id="header" class="clearfix">
  <div class="container">
    <div class="row"> 
      
      <!--logo starts-->
      <div class="span4 logo"><a href="{{url('/')}}"><img src="{{asset('img/logo.png')}}" width="148" height="41" alt="logo"></a></div>
      <!--logo ends-->
      
      <div class="span8 clearfix"> 
        
        <!--social starts-->
        <ul class="social clearfix">
          <li><a href="#"><img src="{{asset('img/icons/social-icon-linkedin.png')}}" width="28" height="28" alt="icon"></a></li>
          <li><a href="#"><img src="{{asset('img/icons/social-icon-facebook.png')}}" width="28" height="28" alt="icon"></a></li>
          <li><a href="#"><img src="{{asset('img/icons/social-icon-twitter.png')}}" width="28" height="28" alt="icon"></a></li>
        </ul>
        <!--social ends--> 
        
        <!--menu starts-->
        <div id="smoothmenu" class="ddsmoothmenu">
          <ul>
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('about')}}">About</a> </li>
            <li><a href="{{url('features')}}">Features</a>              
            </li>
            <li><a href="{{url('contact')}}">Contact</a></li>
            @if (Auth::guest())
              <li><a href="{{ url('/login') }}">Login</a></li>
              <li><a href="{{ url('/register') }}">Register</a></li>
            @else
              <li class="dropdown menu_color">
                <a href="#" class="dropdown-toggle menu_color" data-toggle="dropdown" role="button" aria-expanded="false">
                  {{ Auth::user()->first_name }} <i class="fa fa-caret-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/home') }}"><i class="fa fa-user fa-user"></i>Profile</a></li>
                  <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                </ul>
              </li>
            @endif
          </ul>
        </div>
        <!--menu ends--> 
        
      </div>
    </div>
  </div>
</section>
<!-- header ends
================================================== --> 

<!-- content starts
================================================== -->
<section id="content" class="clearfix"> 
  @yield('content')
</section>
<!-- content ends
================================================== --> 



<!-- footer starts
================================================== -->
<footer id="footer" class="clearfix">
  <div class="container"> 
    
    <!--row starts-->
    <div class="row"> 
      
      <!--column one / social icon list starts-->
      <div class="span6">
        <ul class="social clearfix">
          <li><a href="#"><img src="{{asset('img/icons/social-icon-linkedin.png')}}" width="28" height="28" alt="icon"></a></li>
          <li><a href="#"><img src="{{asset('img/icons/social-icon-facebook.png')}}" width="28" height="28" alt="icon"></a></li>
          <li><a href="#"><img src="{{asset('img/icons/social-icon-twitter.png')}}" width="28" height="28" alt="icon"></a></li>
        </ul>
        <div class="clearfix">
          Follow on Twitter
          <h5><a href="#">@notifyme</a></h5>
        </div>

      </div>
      <!--column one ends--> 
      
      <!--column two starts-->
      <div class="span3 text-center hidden"> 
      </div>
      <!--column two ends--> 
      
      <!--column three / twitter feed starts-->
      <div class="span6">
        <div id="twitter"></div>
      </div>
      <!--column three / twitter feed ends--> 
      
    </div>
    <!--row ends--> 
    
    <!--spacer here-->
    <div class="spacer-30px"></div>
    
    <!--row starts-->
    <div class="row"> 
      
      <!--column one starts-->
      <div class="span6">
        <h2 class="color--blue">About Notifyme</h2>
        <p class="text-justify">NotifyMe.ng is a cloud based incident and mass communication platform (a service of Ecommerce Technology Alliance Ltd) that provides critical event communication to its subscribers via their mobile phone. NotifyMe.ng features Geo-location targeting that allows users to receive critical event information that can save Life, Money, Time and Property based on their specified and surrounding location.</p>
        <img src="{{asset('img/logo.png')}}" width="148" height="41" alt="logo"> </div>
      <!--column one ends--> 
      
      
      <!--column two starts-->
      <div class="span6">
        <h2 class="color--blue">Get in touch</h2>
        <ul class="list-info">
          <li><img src="{{asset('img/icons/icon-18-address.png')}}" alt="icon" style="margin-bottom:30px;">Civic Studios, Inc.<br/>
            3245 East Creek Blvd, Suite 590<br/>
            Bos Boomer, IN 20076 </li>
          <li><img src="{{asset('img/icons/icon-18-phone.png')}}" alt="icon">+234-809-732-1660</li>
          <li><img src="{{asset('img/icons/icon-18-mail.png')}}" alt="icon"><a href="#">noc@notiifyme.ng</a></li>
          <li><img src="{{asset('img/icons/icon-18-skype.png')}}" alt="icon" class="mar-btm"><a href="#">http://skype.com/notifyme.ng</a></li>
        </ul>
      </div>
      <!--column two ends--> 
      
    </div>
    <!--row ends--> 
    
  </div>
</footer>
<!-- footer ends
================================================== --> 

<!-- copyright starts
================================================== -->
<section id="copyright" class="clearfix">
  <div class="container">
    <div class="row">
      <div class="span12">
        <p> © Copyright 2016</p>
        
        <!--copyright menu starts-->
        <ul class="copyright-menu">
          <li><a href="{{url('/')}}">Home</a></li>
          <li><a href="{{url('/features')}}">Features</a></li>
          <li><a href="#">Blog</a></li>
          <li><a href="#">Company</a></li>
          <li><a href="{{url('contact')}}">Contact</a></li>
        </ul>
        <!--copyright menu ends--> 
        
      </div>
    </div>
  </div>
</section>
<!-- copyright ends
================================================== --> 

<!--other jqueries required--> 
<script src="{{asset('js/bootstrap.js')}}" type="text/javascript" ></script> 
<script src="{{asset('js/ddsmoothmenu.js')}}" type="text/javascript" ></script> 
<script src="{{asset('js/jquery.easing.1.3.js')}}" type="text/javascript" ></script> 
<script src="{{asset('js/custom.js')}}" type="text/javascript" ></script>
<script src="{{asset('js/myfunctions.js')}}" type="text/javascript" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js" type="text/javascript" ></script>
<!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/livestamp/1.1.2/livestamp.min.js" type="text/javascript" ></script>

            <script type="text/javascript">
              var save_email_url="{{url('email/save')}}";
              var save_phone_url="{{url('phone/save')}}";
              var save_location_url= "{{url('location/save')}}";
              var latlng_url="{{url('latlong')}}/";
              function downloadJSAtOnload() {
              var element = document.createElement("script");
              element.src = "https://js.paystack.co/v1/inline.js";
              document.body.appendChild(element);
              }
              if (window.addEventListener)
              window.addEventListener("load", downloadJSAtOnload, false);
              else if (window.attachEvent)
              window.attachEvent("onload", downloadJSAtOnload);
              else window.onload = downloadJSAtOnload;
            </script>
            <script>
              function payWithPaystack(){
                /*Generate random hash*/
                var months=$('#months').val();
                console.log(months);
                var xhttp = new XMLHttpRequest();
                  xhttp.onreadystatechange = function() {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                     console.log(xhttp.responseText);
                     var json= JSON.parse(xhttp.responseText);
                     var hash=json.ref;
                     var user_email= json.email;
                     var pay_amount =json.amount;
                     var my_key=json.key;
                    //console.log(hash);
                    //console.log(pay_amount);
                    var handler = PaystackPop.setup({
                      key: my_key,
                      email: user_email,
                      amount: pay_amount,
                      ref: hash,
                      callback: function(response){
                        //console.log(response);
                        $.get("{{url('/payment')}}",
                            {reference: response.trxref},
                            function(data){
                                console.log(data);
                                window.location.reload();
                            });
                        //console.log('success. transaction ref is ' + response.trxref);
                      },
                      onClose: function(){
                          //console.log('window closed');
                      }
                    });
                    handler.openIframe();
                    }
                  };
                  xhttp.open("GET", "{{url('ref_code')}}/"+months, true);
                  xhttp.send();
                /*Generate payment and popup*/


              }
            </script>

</body>
</html>