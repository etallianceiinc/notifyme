<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="assets/images/favicon.ico">

	<title>Notifyme Admin</title>

	<link rel="stylesheet" href="{{asset('assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-icons/entypo/css/entypo.css')}}">
	<!--<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic"> -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-core.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-theme.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/neon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

	<script src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container sidebar-collapsed"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
	
	<div class="sidebar-menu">

		<div class="sidebar-menu-inner">
			
			<header class="logo-env">

				<!-- logo -->
				<div class="logo">
					<a href="{{url('/')}}">Home
					</a>
				</div>

				<!-- logo collapse icon -->
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>

								
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>
			
									
			<ul id="main-menu" class="main-menu">
				<!-- add class "multiple-expanded" to allow multiple submenus to open -->
				<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
				<li class="has-sub">
					<a href="{{url('/logout')}}">
						<i class="entypo-gauge"></i>
						<span class="title">Logout</span>
					</a>
				</li>			
		</div>

	</div>

	<div class="main-content">
				
		
		
		<hr />
		
					<ol class="breadcrumb bc-3" >
								
							<li>
		
									<a href="#">Search users</a>
							</li>
						<li class="active">
		
									<strong>Search users</strong>
							</li>
							</ol>
					
		<h2>Users List</h2>
		
		<br />
			
		<div>
            @foreach($users as $key => $user)  

            {{$user['first_name']}} {{$user['last_name']}}, 
            <div class="make-switch has-switch">
                <div class="switch-off">
                <input type="checkbox" value="1"><span class="switch-left">ON</span><label>&nbsp;</label><span class="switch-right">OFF</span></div>
            </div>
            @endforeach
        </div>
		<h3>Exporting users List</h3>
		<br />
		<script type="text/javascript">
		jQuery( document ).ready( function( $ ) {
			var $table4 = jQuery( "#table-4" );
			
			$table4.DataTable( {
				dom: 'Bfrtip',
				buttons: [
					'copyHtml5',
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5',
                    'colvis'
				]
			} );
		} );		
		</script>
		<table class="table table-bordered datatable" id="table-4">
			<thead>
				<tr>
                    <td scope="col">First Name</td>
                    <td scope="col">Middle Initial</td>
                    <td scope="col">Last Name</td>                    
                    <td scope="col">Suffix</td>
                    <td scope="col">External ID</td>
                    <td scope="col">SSO User ID</td>
                    <td scope="col">Country</td>
                    <td scope="col">Record Type</td>
                    <td scope="col">Groups</td>
                    <td scope="col">Group Remove</td>
                    @for($i=1;$i<=5;$i++)
                    <td scope="col">Location {{$i}}</td>
                    <td scope="col">Street Address {{$i}}</td>
                    <td scope="col">Apt/Suite/Unit {{$i}}</td>
                    <td scope="col">City {{$i}}</td>
                    <td scope="col">State/Province {{$i}}</td>
                    <td scope="col">Postal Code {{$i}}</td>
                    <td scope="col">Country {{$i}}</td>
                    <td scope="col">Latitude {{$i}}</td>
                    <td scope="col">Longitude {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=5;$i++)
                    <td>Extension Phone {{$i}}</td>
                    <td>Extension {{$i}}</td>
                    <td>Extension Phone Country {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=6;$i++)
                    <td>Phone {{$i}}</td>
                    <td>Phone Country {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=3;$i++)
                    <td>Email Address {{$i}}</td>
                    @endfor
                    <td>Plain Text Email - 1 way</td>
                    <td>Plain Text - 1 way Pager Service</td>
                    <td>Plain Text Email - 2 way</td>
                    @for($i=1; $i<=2; $i++)
                        <td>SMS {{$i}}</td>
                        <td>SMS {{$i}} Country</td>
                    @endfor

                    @for($i=1;$i<=3; $i++)
                        <td>FAX {{$i}} </td>
                        <td>FAX Country {{$i}}</td>
                    @endfor

                    @for($i=1;$i<=3; $i++)
                        <td>TTY {{$i}} </td>
                        <td>TTY Country {{$i}}</td>
                    @endfor
                    <td>Numeric Pager</td>
                    <td>Numeric Pager Country</td>
                    <td>Numeric Pager Pin</td>
                    <td>Numeric Pager Service</td>
                    <td>TAP Pager</td>
                    <td>TAP Pager Country</td>
                    <td>TAP Pager Pin</td>

                    <td>One Way SMS</td>
                    <td>One Way SMS Country</td>

                    @for($i=1; $i<=3; $i++)
                        <td>Custom Field {{$i}}</td>
                        <td>Custom Value {{$i}}</td>
                    @endfor
                    <td>END</td>
                </tr> 
			</thead>
			<tbody>
				@if(isset($users))
				@foreach($users as $key => $user)
                @if(strtotime($user['end_of_subscription']) > time())
				<tr class="odd gradeX">
                    <td scope="col">{{$user['first_name']}}</td>
                    <td scope="col"></td>
                    <td scope="col">{{$user['last_name']}}</td>                    
                    <td scope="col"></td>
                    <td scope="col">{{md5($user['id'].$user['first_name'])}}</td><!-- external ID -->
                    <td scope="col"></td>
                    <td scope="col"></td>
                    <td scope="col">Subscriber</td><!-- Record type -->
                    <td scope="col"></td>
                    <td scope="col"></td>
                        
                    <!-- Locations of user -->
                    
                    @for($i=0;$i< count($user['locations']);$i++)
                        <td scope="col">{{$user['locations'][$i]['location'] ? $user['locations'][$i]['location'] : ' '}}</td>
                        <td scope="col">{{$user['locations'][$i]['street_address'] ? $user['locations'][$i]['street_address'] : ' '}}</td>
                        <td scope="col"></td>
                        <td scope="col">{{$user['locations'][$i]['city'] ? $user['locations'][$i]['city'] : ' '}}</td>
                        <td scope="col">{{$user['locations'][$i]['state'] ? $user['locations'][$i]['state'] : ' '}}</td>
                        <td scope="col"></td>
                        <td scope="col">{{$user['locations'][$i]['country'] ? $user['locations'][$i]['country'] : ' '}}</td>
                        <td scope="col">{{$user['locations'][$i]['latitude'] ? $user['locations'][$i]['latitude'] : ' '}}</td>
                        <td scope="col">{{$user['locations'][$i]['longitude'] ? $user['locations'][$i]['longitude'] : ' '}}</td>
                    @endfor

                    @for($i=count($user['locations']); $i < 5 ; $i++)
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                        <td scope="col"></td>
                    @endfor

                    <!-- Extension phone -->
                    @for($i=1;$i<=5;$i++)
                    <td></td>
                    <td></td>
                    <td></td>
                    @endfor

                    <!-- Phone -->
                    @for($i=0;$i< count($user['phones']);$i++)
                    <td>{{$user['phones'][$i]['phone'] ? $user['phones'][$i]['phone'] : ' '}}</td>
                    <td>{{$user['phones'][$i]['phone_country'] ? $user['phones'][$i]['phone_country'] : ' '}}</td>
                    @endfor

                    @for($i=count($user['phones']); $i < 6 ; $i++)
                        <td scope="col"></td>
                        <td scope="col"></td>                    
                    @endfor


                    <!-- Email -->
                    @for($i=0;$i< count($user['emails']);$i++)
                    <td>{{$user['emails'][$i]['email'] ? $user['emails'][$i]['email'] : ' '}}</td>
                    @endfor

                    @for($i=count($user['emails']); $i < 3 ; $i++)
                        <td scope="col"></td>                
                    @endfor


                    <td></td>
                    <td></td>
                    <td></td>

                    <!-- SMS -->
                        @for($i=0; $i<= 1; $i++)
                            <td>@if(array_key_exists($i,$user['phones'])) {{$user['phones'][$i]['phone']}} @endif</td>
                            <td>@if(array_key_exists($i,$user['phones'])) {{$user['phones'][$i]['phone_country']}} @endif</td>
                        @endfor

                    <!-- FAX -->
                    @for($i=1;$i<=3; $i++)
                        <td></td>
                        <td></td>
                    @endfor
                    <!-- TTY -->
                    @for($i=1;$i<=3; $i++)
                        <td></td>
                        <td></td>
                    @endfor

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <!-- Custom field -->
                    @for($i=1; $i<=3; $i++)
                        <td></td>
                        <td></td>
                    @endfor
                    <td>END</td>
				</tr>
                @endif
				@endforeach
				@endif
				
			</tbody>
			<tfoot>
                <tr>
                    <td scope="col">First Name</td>
                    <td scope="col">Middle Initial</td>
                    <td scope="col">Last Name</td>                    
                    <td scope="col">Suffix</td>
                    <td scope="col">External ID</td>
                    <td scope="col">SSO User ID</td>
                    <td scope="col">Country</td>
                    <td scope="col">Record Type</td>
                    <td scope="col">Groups</td>
                    <td scope="col">Group Remove</td>
                    @for($i=1;$i<=5;$i++)
                    <td scope="col">Location {{$i}}</td>
                    <td scope="col">Street Address {{$i}}</td>
                    <td scope="col">Apt/Suite/Unit {{$i}}</td>
                    <td scope="col">City {{$i}}</td>
                    <td scope="col">State/Province {{$i}}</td>
                    <td scope="col">Postal Code {{$i}}</td>
                    <td scope="col">Country {{$i}}</td>
                    <td scope="col">Latitude {{$i}}</td>
                    <td scope="col">Longitude {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=5;$i++)
                    <td>Extension Phone {{$i}}</td>
                    <td>Extension {{$i}}</td>
                    <td>Extension Phone Country {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=6;$i++)
                    <td>Phone {{$i}}</td>
                    <td>Phone Country {{$i}}</td>
                    @endfor
                    @for($i=1;$i<=3;$i++)
                    <td>Email Address {{$i}}</td>
                    @endfor
                    <td>Plain Text Email - 1 way</td>
                    <td>Plain Text - 1 way Pager Service</td>
                    <td>Plain Text Email - 2 way</td>
                    @for($i=1; $i<=2; $i++)
                        <td>SMS {{$i}}</td>
                        <td>SMS {{$i}} Country</td>
                    @endfor

                    @for($i=1;$i<=3; $i++)
                        <td>FAX {{$i}} </td>
                        <td>FAX Country {{$i}}</td>
                    @endfor

                    @for($i=1;$i<=3; $i++)
                        <td>TTY {{$i}} </td>
                        <td>TTY Country {{$i}}</td>
                    @endfor
                    <td>Numeric Pager</td>
                    <td>Numeric Pager Country</td>
                    <td>Numeric Pager Pin</td>
                    <td>Numeric Pager Service</td>
                    <td>TAP Pager</td>
                    <td>TAP Pager Country</td>
                    <td>TAP Pager Pin</td>

                    <td>One Way SMS</td>
                    <td>One Way SMS Country</td>

                    @for($i=1;$i<=3; $i++)
                        <td>Custom Field {{$i}}</td>
                        <td>Custom Value {{$i}}</td>
                    @endfor
                    <td>END</td>
                </tr>
			</tfoot>
		</table>
		
		<br />
		<!-- Footer -->
		<footer class="main">
			
			&copy; 2016 <strong>Notifyme</strong> Admin by <a href="#" >Kunle</a>
		
		</footer>
	</div>
	
</div>





	<!-- Imported styles on this page -->
	<link rel="stylesheet" href="{{asset('assets/js/datatables/datatables.css')}}">
	<link rel="stylesheet" href="{{asset('assets/js/select2/select2-bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/js/select2/select2.css')}}">

	<!-- Bottom scripts (common) -->
	<script src="{{asset('assets/js/gsap/TweenMax.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/js/joinable.js')}}"></script>
	<script src="{{asset('assets/js/resizeable.js')}}"></script>
	<script src="{{asset('assets/js/neon-api.js')}}"></script>


	<!-- Imported scripts on this page -->
	<script src="{{asset('assets/js/datatables/datatables.js')}}"></script>
	<script src="{{asset('assets/js/select2/select2.min.js')}}"></script>
	<script src="{{asset('assets/js/neon-chat.js')}}"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="{{asset('assets/js/neon-custom.js')}}"></script>


	<!-- Demo Settings -->
	<script src="{{asset('assets/js/neon-demo.js')}}"></script>

</body>
</html>