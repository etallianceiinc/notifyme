<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/css/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/style.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/css/dark.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/css/font-icons.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/css/animate.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('canvas/css/magnific-popup.css')}}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="shortcut icon" href="{{asset('favico.png')}}">
    <link rel="stylesheet" href="{{asset('canvas/css/responsive.css')}}" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- External JavaScripts
    ============================================= -->
	<script type="text/javascript" src="{{asset('canvas/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('canvas/js/plugins.js')}}"></script>

    <!-- Document Title
    ============================================= -->
	<title>Notifyme</title>

</head>

<body class="stretched">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->
        <header id="header" class="light full-header" data-sticky-class="not-dark">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="{{url('/')}}" class="standard-logo" data-dark-logo="{{asset('logo.png')}}"><img src="{{asset('logo.png')}}" alt="Canvas Logo"></a>
                        <a href="{{url('/')}}" class="retina-logo" data-dark-logo="{{asset('logo.png')}}"><img src="{{asset('logo.png')}}" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul>
                            <li class="@if(url()->current()=='/') current @endif"><a href="{{url('/')}}"><div>Home</div></a>                                
                            </li>
                        @if (Auth::guest())
                            <li class="@if(url()->current()=='/login') current @endif"><a href="{{url('/login')}}"><div>Login/Register</div></a>                                
                            </li>
                        </ul>
                        @else

                        <!-- Top Cart
                        ============================================= -->
                        <div id="top-cart">
                            <a href="#" id="top-cart-trigger"><i class="icon-users"></i> Account</a>
                            <div class="top-cart-content">
                                <div class="top-cart-title">
                                    <h4>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h4>
                                </div>
                                <div class="top-cart-items">
                                    <span class="expire_date bold">Subscription Active Till {{date_format(date_create(Auth::user()->end_of_subscription),"dS-M-Y")}}</span> 
                                </div>
                                <a href="{{url('home')}}" class="button--red button button-rounded">View Profile</a>
                                <div class="top-cart-action clearfix">
                                    <a href="{{url('logout')}}"><span class="fleft top-checkout-price">Logout</span></a>
                                </div>
                            </div>
                        </div><!-- #top-cart end -->
                        @endif

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

        @yield('contents')

        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                    <div class="col_half"  style="background: url('{{asset('canvas/images/world-map.png')}}') no-repeat center center; background-size: 100%;">

                            <div class="widget clearfix">

                                <img src="{{asset('logo_M.png')}}" alt="" class="footer-logo">
                                    <p>NotifyMe.ng is a cloud based incident and mass communication platform (a service of Ecommerce Technology Alliance Ltd) that provides critical event communication to its subscribers via their mobile phone.</p>
                                    
                                <div>                                   
                                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> +234-909-365-5982<br> 
                                    <abbr title="Email Address"><strong>Email:</strong></abbr> contact@notifyme.ng
                                </div>

                            </div>

                    </div>

                    <div class="col_half col_last">

                        <div class="widget subscribe-widget clearfix">
                            <h5><strong>Talk</strong> to Us:</h5>
                            <div id="widget-subscribe-form-result" data-notify-type="success" data-notify-msg="Thanks for contacting Us. Our Team will respond swiftly"></div>
                            <form id="widget-subscribe-form" action="{{url('contact/mail')}}" role="form" method="post" class="nobottommargin">
                                <div class="input-group divcenter">
                                    <span class="input-group-addon"><i class="icon-email2"></i></span>
                                    <input type="email" id="widget-subscribe-form-email" name="email" class="form-control required email" placeholder="Enter your Email">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit">Send</button>
                                    </span>
                                </div>
                                <div class="col_full">
                                    <textarea class="required sm-form-control" id="template-contactform-message" placeholder="Type Your Message Here" required name="message" rows="6" cols="30" aria-required="true"></textarea>
                                </div>
                            </form>
                            <script type="text/javascript">
                                $("#widget-subscribe-form").validate({
                                    submitHandler: function(form) {
                                        $(form).find('.input-group-addon').find('.icon-email2').removeClass('icon-email2').addClass('icon-line-loader icon-spin');
                                        $(form).ajaxSubmit({
                                            target: '#widget-subscribe-form-result',
                                            success: function() {
                                                $(form).find('.input-group-addon').find('.icon-line-loader').removeClass('icon-line-loader icon-spin').addClass('icon-email2');
                                                $('#widget-subscribe-form').find('.form-control').val('');
                                                $('#widget-subscribe-form').find('#template-contactform-message').val('');
                                                $('#widget-subscribe-form-result').attr('data-notify-msg', $('#widget-subscribe-form-result').html()).html('');
                                                SEMICOLON.widget.notifications($('#widget-subscribe-form-result'));
                                            }
                                        });
                                    }
                                });
                            </script>
                        </div>

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                            <div class="row">

                                <div class="col-md-6 clearfix bottommargin-sm">
                                    <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                                </div>
                                <div class="col-md-6 clearfix">
                                    <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Follow</strong><br>us on Twitter</small></a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; 2016 All Rights Reserved by Notifyme Inc.<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> contact@notifyme.ng <span class="middot">&middot;</span> <i class="icon-headphones"></i> +234-909-365-5982
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- Footer Scripts
    ============================================= -->
    <script type="text/javascript" src="{{asset('canvas/js/functions.js')}}"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{asset('js/myfunctions.js')}}" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js" type="text/javascript" ></script>
    <!-- <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/livestamp/1.1.2/livestamp.min.js" type="text/javascript" ></script>

            <script type="text/javascript">
              var save_email_url="{{url('email/save')}}";
              var save_phone_url="{{url('phone/save')}}";
              var save_location_url= "{{url('location/save')}}";
              var latlng_url="{{url('latlong')}}/";
              function downloadJSAtOnload() {
              var element = document.createElement("script");
              element.src = "https://js.paystack.co/v1/inline.js";
              document.body.appendChild(element);
              }
              if (window.addEventListener)
              window.addEventListener("load", downloadJSAtOnload, false);
              else if (window.attachEvent)
              window.attachEvent("onload", downloadJSAtOnload);
              else window.onload = downloadJSAtOnload;
            </script>
            <script>
              $('#annual').click(function(){
                /*Generate random hash*/
                var months= 12;
                console.log(months);
                var xhttp = new XMLHttpRequest();
                  xhttp.onreadystatechange = function() {
                    if (xhttp.readyState == 4 && xhttp.status == 200) {
                     console.log(xhttp.responseText);
                     var json= JSON.parse(xhttp.responseText);
                     var hash=json.ref;
                     var user_email= json.email;
                     var pay_amount =json.amount;
                     var plan =json.plan;
                     var my_key=json.key;
                    //console.log(hash);
                    //console.log(pay_amount);
                    var handler = PaystackPop.setup({
                      key: my_key,
                      email: user_email,
                      amount: pay_amount,
                      ref: hash,
                        plan: plan,
                      callback: function(response){
                        //console.log(response);
                        $.get("{{url('/payment')}}",
                            {reference: response.trxref},
                            function(data){
                                console.log(data);
                                window.location.reload();
                            });
                        //console.log('success. transaction ref is ' + response.trxref);
                      },
                      onClose: function(){
                          //console.log('window closed');
                      }
                    });
                    handler.openIframe();
                    }
                  };
                  xhttp.open("GET", "{{url('ref_code')}}/"+months, true);
                  xhttp.send();
                /*Generate payment and popup*/
              });

              $('#monthly').click(function(){
                  /*Generate random hash*/
                  //console.log(months);
                  var months= 1;
                  console.log(months);
                  var xhttp = new XMLHttpRequest();
                  xhttp.onreadystatechange = function() {
                      if (xhttp.readyState == 4 && xhttp.status == 200) {
                          console.log(xhttp.responseText);
                          var json= JSON.parse(xhttp.responseText);
                          var hash=json.ref;
                          var user_email= json.email;
                          var pay_amount =json.amount;
                          var plan =json.plan;
                          var my_key=json.key;
                          //console.log(hash);
                          //console.log(pay_amount);
                          var handler = PaystackPop.setup({
                              key: my_key,
                              email: user_email,
                              amount: pay_amount,
                              ref: hash,
                              plan: plan,
                              callback: function(response){
                                  //console.log(response);
                                  $.get("{{url('/payment')}}",
                                          {reference: response.trxref},
                                          function(data){
                                              console.log(data);
                                              window.location.reload();
                                          });
                                  //console.log('success. transaction ref is ' + response.trxref);
                              },
                              onClose: function(){
                                  //console.log('window closed');
                              }
                          });
                          handler.openIframe();
                      }
                  };
                  xhttp.open("GET", "{{url('ref_code')}}/"+months, true);
                  xhttp.send();
                  /*Generate payment and popup*/
              });
            </script>


</body>
</html>