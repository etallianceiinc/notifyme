@extends('layouts.canvas')
@section('contents')

  <!-- Page Title
    ============================================= -->  
  <section id="page-title">

      <div class="container clearfix">
        <h1>Dashboard</h1>
        <span class="hidden">Hi <b class="capitalize">{{ Auth::user()->first_name }}</b></span>
        <ol class="breadcrumb">
          <li><a href="{{url('/')}}">Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </div>

    </section><!-- #page-title end -->
  <section id="content">
    <div class="content-wrap">

      <div class="container clearfix">
        <div class="promo promo-dark bottommargin promo-center">
            <h3>Hi <span class="capitalize">{{ Auth::user()->first_name }}</span>, @if($status=='active') Your @if(Auth::user()->trial==1) Trial Period @else Subscription @endif Expires <span>{{date_format(date_create(Auth::user()->end_of_subscription),"dS-M-Y")}}</span>
                @else Your Subscription Has Expired!
                @endif</h3>
            <div>
              <h4 class="text--white">Renew Subscription</h4>
              <div class="col_one_fourth"></div>
              <div class="col_one_fourth">
                <select name="months" id="months" class="text--black form-control">
                  <option selected value="1">1 month</option>
                  @for($i = 2; $i < 12; $i++)
                    <option value="{{$i}}">{{$i}} months</option>
                  @endfor
                    <option value="12">1 year</option>
                </select> 
              </div>
            </div>
            <a href="#promo" class="button--red button button-xlarge button-rounded" onclick="payWithPaystack()">Make Payment</a>
        </div>
        <div class="container">
                        @include('common.notify')

                  
            <div class="row">
              <div class="col-md-10 col-md-offset-1 col-xs-offset-1">   
                <div class="tabs tabs-alt tabs-justify tabs-tb clearfix" id="tab-10">

                    <ul class="tab-nav clearfix">
                      <li><a href="#tabs-37">Profile Details</a></li>
                      <li><a href="#tabs-38">Location</a></li>
                      <li><a href="#tabs-39">Email</a></li>
                      <li class="hidden-phone"><a href="#tabs-40">Phone Numbers</a></li>
                    </ul>

                    <div class="tab-container">

                      <div class="tab-content clearfix" id="tabs-37">
                        <p>
                          <form  id="contactform" method="post" action="{{url('user/update')}}">
                            <fieldset>
                              {!! csrf_field() !!}
                              <div class="row">
                                <div class="col-md-5">
                                  <label>First Name :</label>
                                  <input name="first_name" class="form-control"  type="text" value="{{Auth::user()->first_name}}">
                                </div>
                                <div class="col-md-5">
                                  <label>Last Name :</label>
                                  <input name="last_name" class="form-control" type="text" value="{{Auth::user()->last_name}}">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-5">
                                  <label>Contact Phone Number :</label>
                                  <input name="phone" class="number form-control" type="phone" value="{{Auth::user()->phone}}">
                                </div>
                                <div class="col-md-5">
                                  <label>Contact Email Address :</label>
                                  <input name="email"  class="required email form-control" type="text" value="{{Auth::user()->email}}">
                                </div>
                              </div>
                              <div class="row">
                                <input type="submit" value="Update" class="submit button"/>
                              </div>
                              <div class="clearfix"></div>
                              <div id="result"></div>
                            </fieldset>
                          </form>
                        </p>
                      </div>
                      <div class="tab-content clearfix" id="tabs-38">
                        <div class="location_category">                                      
                          @for($i=0; $i<=4; $i++)
                            <div class="hidden">{{$no=$i+1}}</div>
                            <form class="form-inline" action="{{url('location/save')}}" role="form">
                              <div class="form-group">
                                <div class="span6">                          
                                  <label for="location" class="mylabel">Location {{$i+1}}:</label>
                                  <input type="text" class="form-control" class="form-control" id="location" data-id="{{$no}}" disabled value="@if(isset($location_type[$i])) {{$location_type[$i]}} @endif">
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="span6">                          
                                  <label for="street_address" class="mylabel">Street Address:</label>
                                  <input type="text" class="form-control" id="street_address" data-id="{{$i+1}}" value="@if(isset($user['locations'][$i])) {{$user['locations'][$i]['street_address']}} @endif">
                                </div>
                              </div>
                              <div class="form-group">
                                <div class="span12">
                                  <label for="city">City:</label>
                                  <input type="text" class="form-control" id="city" data-id="{{$i+1}}" value="@if(isset($user['locations'][$i])) {{$user['locations'][$i]['city']}} @endif">   

                                  <label for="state">State</label>
                                  <select class="country-select form-control" name="state" id="state">
                                    @foreach($states as $state)
                                      <option value="{{$state['name']}}" data-lat="{{$state['latitude']}}" data-log="{{$state['longitude']}}" @if(array_key_exists($i,$user['locations']) && $state['name'] == $user['locations'][$i]['state']) selected @endif >{{$state['name']}}</option>
                                    @endforeach
                                  </select>

                                </div>
                              </div>
                              <div class="span6">
                                <button type="button"  class="location-submit btn btn-default" data-id="{{$i+1}}">Save</button>
                                <span class='btn-success bold saving hidden'>Saving...</span>
                              </div>
                            </form>
                          @endfor
                        </div>
                      </div>
                      
                      <div class="tab-content clearfix" id="tabs-39">
                        <div class="email_category">
                          @for($i=0; $i<=2; $i++)
                          <form class="form-inline" action="{{url('email/save')}}" role="form">
                            <div class="form-group">
                              <label for="email">Email address {{$i+1}}:</label>
                              <input type="email" class="form-control" id="email" value="@if(array_key_exists($i,$user['emails'])) {{$user['emails'][$i]['email']}} @endif" data-id="{{$i+1}}">
                              <button type="button"  class="email-submit btn btn-default">Save</button>
                              <span class='btn-success bold saving hidden'>Saving...</span>
                            </div>
                          </form>
                          <br>
                          @endfor
                        </div>
                      </div>
                      <div class="tab-content clearfix" id="tabs-40">
                        <div class="phone_category">
                          @for($i=0; $i<=5; $i++)
                          <form class="form-inline" action="{{url('phone/save')}}" role="form">
                            <div class="form-group">
                              <!-- Country Codes -->
                              <select class="country-select form-control" name="phone_country" id="phone_country">
                                @foreach($country_codes as $country)
                                  <option
                                    @if(array_key_exists($i,$user['phones']) && $country['altSpellings']['0'] == $user['phones'][$i]['phone_country']) selected @endif
                                  value="{{$country['altSpellings']['0']}}">{{$country['name']}}</option>                            
                                @endforeach
                              </select>
                              <label for="phone">Phone Number {{$i+1}}:</label>
                              <input type="number" class="form-control" id="phone" data-id="{{$i+1}}" value="@if(array_key_exists($i,$user['phones'])){{$user['phones'][$i]['phone']}}@endif">                        
                              <button type="button"  class="phone-submit btn btn-default">Save</button>
                              <span class='btn-success bold saving hidden'>Saving...</span>
                            </div>
                          </form>
                          <br>
                          @endfor
                        </div>
                      </div>

                    </div>

                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
  </section>
@endsection
