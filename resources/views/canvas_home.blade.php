@extends('layouts.canvas')
@section('contents')

        <section id="slider" class="slider-parallax" style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('{{asset('stock/fire.jpg')}}') no-repeat; background-size: cover" data-height-lg="600" data-height-md="500" data-height-sm="400" data-height-xs="300" data-height-xxs="250">
            <div class="container clearfix">
                <div class="vertical-middle dark center">

                    <div class="heading-block nobottommargin center">
                        <h1>
                            <span class="text-rotater nocolor" data-separator="|" data-rotate="flipInX" data-speed="3500">
                                 Receive critical event information that saves <span class="t-rotate">Life|Time|Money|Property</span>
                            </span>
                        </h1>
                        <span class="hidden-xs">You will receive critical events communication by SMS, email, Push Notification and voice communication.</span>
                    </div>

                    <a href="{{url('login')}}" class="button button-border button-light button-rounded button-reveal tright button-large topmargin hidden-xs"><i class="icon-angle-right"></i><span>Join Notifyme</span></a>

                </div>
            </div>
        </section>

<!-- Content
        ============================================= -->
        <section id="content">
                
            <a href="{{url('login')}}" class="button button-full center tright footer-stick">
                <div class="container clearfix">
                    <b>#Be</b> In the Know Anywhere in <b>Nigeria</b> <strong>Join Notifyme</strong> <i class="icon-caret-right" style="top:4px;"></i>
                </div>
            </a>

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="col_one_third">
                        <div class="feature-box fbox-border fbox-effect">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-email"></i></a>
                            </div>
                            <h3>Email Notification</h3>
                            <p>Powerful built email communication system.</p>
                        </div>
                    </div>

                    <div class="col_one_third">
                        <div class="feature-box fbox-border fbox-effect">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-comment"></i></a>
                            </div>
                            <h3>SMS Notification</h3>
                            <p>Messages directly to your phone number.</p>
                        </div>
                    </div>

                    <div class="col_one_third col_last">
                        <div class="feature-box fbox-border fbox-effect">
                            <div class="fbox-icon">
                                <a href="#"><i class="icon-fast-forward"></i></a>
                            </div>
                            <h3>Instant Alert</h3>
                            <p>As it happens, you are informed.</p>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>

                <div class="promo topmargin-lg promo-border bottommargin-lg promo-full">
                    <div class="container clearfix">
                        <h3>
                        Get started today with a free trial!</h3>
                        <span><b>30 days</b> free trial | <b>₦500/mo</b> after end of trial period</span>
                        <a href="{{url('login')}}" class="button--red button button-xlarge button-rounded">Join NotifyMe</a>
                    </div>
                </div>

                <div class="container clearfix">
                    <div class="row clearfix">

                        <div class="col-lg-5">
                            <div class="heading-block topmargin">
                                <h1>About NotifyMe.</h1>
                            </div>
                            <p class=""><b>NotifyMe.ng</b> is a cloud based incident and mass communication platform that provides critical event communication to its subscribers via their mobile phone. 
                            <br>NotifyMe.ng features Geo-location targeting that allow users to receive critical event information that can save life, time and money based on their current and surrounding location. </p>
                        </div>

                        <div class="col-lg-7">

                            <div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-lg="426" data-height-md="567" data-height-sm="470" data-height-xs="287" data-height-xxs="183">
                                <img src="{{asset('stock/comm1.jpg')}}" data-animate="fadeInUp" data-delay="400" alt="iPad">
                                <img src="{{asset('logo_M.png')}}" data-animate="fadeInUp" data-delay="100" alt="Chrome">
                            </div>

                        </div>

                    </div>
                </div>

                <!-- <div class="container clearfix">

                    <div id="side-navigation">

                        <div class="col_one_third nobottommargin">

                            <ul class="sidenav">
                                <li class="ui-tabs-active"><a href="#snav-content1"><i class="icon-screen"></i>Responsive Layout<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content2"><i class="icon-magic"></i>Retina Ready Display<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content3"><i class="icon-star3"></i>Fast Performance<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content4"><i class="icon-gift"></i>Bootstrap 3.2 Compatible<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content5"><i class="icon-adjust"></i>Light &amp; Dark Scheme<i class="icon-chevron-right"></i></a></li>
                            </ul>

                        </div>

                        <div class="col_two_third col_last nobottommargin">

                            <div id="snav-content1">
                                <div class="heading-block">
                                    <h3>Flexible Responsive Layout</h3>
                                    <span>We support multiple Devices &amp; Layouts.</span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo rerum facilis deserunt maxime perspiciatis suscipit numquam ipsam, quisquam nesciunt, expedita voluptas et placeat odio nisi dolorum. Ab minus nam tenetur accusamus eligendi maiores natus ipsum ratione possimus a, nostrum atque!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur tempora perferendis maiores saepe voluptatibus possimus, voluptatum laborum. Veritatis deleniti expedita veniam quo eum commodi laboriosam illo obcaecati sit in, illum saepe neque voluptas quis, ullam porro autem. Qui incidunt amet eum dolores expedita, sit laudantium, saepe. Nam tempore rerum, quibusdam est quia impedit rem unde nostrum voluptatum minus ipsa quam fugiat ullam voluptatibus neque accusamus modi eos veniam. Dolor, reiciendis.</p>
                            </div>

                            <div id="snav-content2">
                                <div class="heading-block">
                                    <h3>Brilliant Retina Display</h3>
                                    <span>Crisp &amp; Clear Graphics across all your Retina &amp; Standard Devices.</span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis praesentium quaerat blanditiis incidunt corporis, odit placeat dolorum. Voluptate laborum facere reprehenderit dolores repudiandae voluptatibus earum quod dignissimos odit! Aperiam optio delectus dolorum, fugit praesentium aut voluptate autem, sapiente labore architecto magni cumque magnam nobis, error accusamus soluta. Facilis, architecto, eos!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum consequuntur quaerat vero qui ipsum sunt velit vel, officia, nihil amet ullam. Omnis nam rerum, harum!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima perspiciatis cumque, ipsa laudantium totam blanditiis, expedita ut odit, cupiditate rem facilis ea ab, hic amet numquam nulla possimus consectetur ipsum fuga! Atque quibusdam, id eius illum numquam porro architecto accusamus nam adipisci mollitia excepturi dolores non, maiores sit fuga vero cumque ullam. Vitae quidem totam similique tempore eligendi necessitatibus culpa.</p>
                            </div>

                            <div id="snav-content3">
                                <div class="heading-block">
                                    <h3>Powerful &amp; Optimized Code</h3>
                                    <span>Smart Reusable code all throughout the Template for Infinite Customizations.</span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit perferendis quisquam quod eius illo possimus id maiores consequatur accusamus commodi?</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, optio animi adipisci distinctio fuga, nihil perferendis dignissimos. Aperiam odio voluptatem dignissimos aliquam? Ea, nihil animi, recusandae quos nulla, suscipit magni provident nesciunt incidunt quo dicta asperiores iste. Illo, qui culpa. Reprehenderit in asperiores blanditiis pariatur aliquid iusto. Quisquam voluptatibus nostrum architecto repudiandae voluptate, ipsum quae iure! Commodi recusandae, repellendus voluptas fugiat aspernatur culpa quod delectus quidem consequatur odio assumenda saepe inventore molestiae ea expedita hic dolorum distinctio ut! Maiores, error.</p>
                            </div>

                            <div id="snav-content4">
                                <div class="heading-block">
                                    <h3>Bootstrap Compatible</h3>
                                    <span>Use the amazing features of the Bootstrap Framework.</span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam facilis reiciendis maxime minus, iusto molestiae dolor veritatis officia cumque a sit, ducimus iste beatae eos harum fugit. Fuga et, iste sequi ea quibusdam doloremque delectus. Ducimus delectus incidunt, deleniti assumenda dignissimos magni, quo laboriosam architecto dolorum, facere cum tempore, totam.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum esse odio, voluptatem, fuga dolore labore. Voluptate, tenetur. Ab temporibus sed adipisci dolor ipsa cumque iusto maiores dolorum molestias magni expedita pariatur modi facere cupiditate eius quas, officiis eligendi cum veritatis dolore autem? Inventore vero doloremque sunt, et modi eos placeat.</p>
                            </div>

                            <div id="snav-content5">
                                <div class="heading-block">
                                    <h3>Light &amp; Dark Schemes</h3>
                                    <span>Use the dual tone schemes or mix them according to your needs.</span>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium voluptas eveniet, recusandae ducimus commodi, maiores voluptatem consequuntur autem quod id molestiae quam nulla minus aliquam nobis laboriosam nisi ut aut adipisci esse omnis at. Voluptatem, natus distinctio minus possimus, aliquid magnam ratione. Adipisci odit nemo voluptatum quas animi, amet et fuga quisquam possimus, dolore id sint eum consequuntur, magnam aliquid impedit doloremque voluptates, ducimus laboriosam. Sint quisquam molestias libero voluptatum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur nisi nemo et esse. Fugiat facere harum, error iusto assumenda illo debitis quas, fugit similique minima.</p>
                            </div>

                        </div>

                    </div>

                    <script>
                      $(function() {
                        $( "#side-navigation" ).tabs({ show: { effect: "fade", duration: 400 } });
                      });
                    </script>

                    <div class="clear"></div><div class="line"></div>
                </div> -->
                
                <a href="{{url('login')}}" class="button button-full center tright footer-stick">
                        <div class="container clearfix">
                            Get <b>30Days</b> free trial <strong>Join Notifyme</strong> <i class="icon-caret-right" style="top:4px;"></i>
                        </div>
                </a>
            </div>
            
        </section><!-- #content end -->

        @stop