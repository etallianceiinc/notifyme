@extends('layouts.notifyme')

@section('content')
<div class="container">     
    <!--about image with caption starts-->
    <div class="row">
      <div class="span12 relativeto"> <img src="img/preview/img-about.jpg" alt="about">
        <div class="about-overlay">
          <div class="overlay-inner">
            <ul class="list-unstyled">
              <li>
                <h3>1. Web experiences.</h3>
                Duis autem vel eum iriure dolor in hendrerit in vulputate velit  quam littera legentis esse molestie consequat. </li>
              <li>
                <h3>2. Powerful options.</h3>
                Typi non habent claritatem insitam; est usus legentis in iis iriure qui facit.</li>
              <li>
                <h3>3. Easy customization.</h3>
                Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum est gothica. </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!--about image ends--> 
    
    <!--spacer here-->
    <div class="spacer-30px"></div>
    
    <!--features starts-->
    <div class="row">
      <div class="span12">
        <h1>Why we <span class="color">standout</span> in a crowd?</h1>
        <ul class="list-3col">
          <li>
            <h4>Project Selection</h4>
            <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat praesent 
              luptatum zzril delenit augue dolore te feugait nulla.</p>
            <a href="#">Read more &raquo;</a></li>
          <li>
            <h4>Philosophy</h4>
            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Usus legentis in iis qui facit eorum claritatem.</p>
            <a href="#">Read more &raquo;</a></li>
          <li>
            <h4>Core Ideas</h4>
            <p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum notare quam littera, quam nunc putamus parum claram formas per seacula. </p>
            <a href="#">Read more &raquo;</a></li>
        </ul>
      </div>
    </div>
    <!--features ends--> 
    
    <!--divider here-->
    <div class="spacer-40px"></div>
    
    <!--team starts-->
    <div class="row">
      <div class="span12"> 
        
        <!--icon and heading-->
        
        <h4 class="heading-icon clearfix"> <img src="img/icons/heading-icon-user2.png" width="40" height="40" alt="icon" class="icon-small-bg"> The Team</h4>
        
        <!--team list starts-->
        <ul class="list-4col team">
          <li class="clearfix"> <img src="img/preview/member-1.jpg" alt="image">
            <h5>Marlone Charlson <span>Director, Notifyme Corporation</span></h5>
            <blockquote>
              <p>"molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan"</p>
            </blockquote>
            Paesent luptatum zzril delnit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend tion congue nihil imperdiet.</li>
          <li class="clearfix"> <img src="img/preview/member-2.jpg" alt="image">
            <h5>Alizee Anderson<span>CEO, Notifyme Corporation</span></h5>
            <blockquote>
              <p>"nihil imperdiet doming id quod mazim placerat possim assum"</p>
            </blockquote>
            Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem quod ii legunt saepius. Claritas est etiam processus dynamicus sequitur mutationem consuetudium.</li>
          <li class="clearfix"> <img src="img/preview/member-3.jpg" alt="image">
            <h5>K. Nichole<span>Creative head, Notifyme Corporation</span></h5>
            <blockquote>
              <p>"molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan"</p>
            </blockquote>
            Luptatum zzril delnit paesent augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend tion congue nihil imperdiet.</li>
          <li class="clearfix"> <img src="img/preview/member-4.jpg" alt="image">
            <h5>Jorge B. Vincee<span>Marketing head, Notifyme Corporation</span></h5>
            <blockquote>
              <p>"duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat"</p>
            </blockquote>
            Paesent luptatum zzril delnit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend tion congue nihil imperdiet.</li>
        </ul>
        <!--team list ends--> 
        
      </div>
    </div>
    <!--team ends--> 
    
    <!--spacer here-->
    <div class="spacer-30px"></div>
    
    <!--box starts-->
    <div class="row">
      <div class="span12">
        <div class="dark-box">
          <h1>Headstart your <span class="color">career</span> with us</h1>
          <p>Find out more about <a href="#">current openings</a> & <a href="#">walk-in interviews</a> at Notifyme</p>
        </div>
      </div>
    </div>
    <!--box ends--> 
    
</div>
@endsection
