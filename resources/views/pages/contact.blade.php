@extends('layouts.notifyme')

@section('content')
<div class="container">
    <div class="row"> 
      
      <!--main content starts-->
      <div class="main-content span8">
        <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu facilisis at vero feugiat nulla facilisis at vero eros et accumsan.</p>
        <form  id="contactform" method="post" action="submit-form.php">
          <fieldset>
            <p>
              <label>Full Name :</label>
              <input name="name"  type="text">
            </p>
            <p>
              <label>Company Name :</label>
              <input name="company"  type="text">
            </p>
            <p>
              <label>Telephone Number :</label>
              <input name="phone" class="number"  type="text">
            </p>
            <p>
              <label>Email Address :</label>
              <input name="email"  class="required email" type="text">
            </p>
            <p class="antispam">Leave this empty: <br />
              <input name="url" />
            </p>
            <p>
              <label>Message :</label>
              <textarea  rows="4" name="message" id="message" class="required"></textarea>
            </p>
            <p>
              <input type="submit" value="Send Message" class="submit"/>
            </p>
            <div class="clearfix"></div>
            <div id="result"></div>
          </fieldset>
        </form>
      </div>
      <!--main content ends--> 
      
      <!--sidebar starts-->
      <div class="sidebar span4 contact">
        <h4>Product Sales & Support</h4>
        <p>For bulk and custom works contact us by mail or phone. </p>
        <h5>Sales inquiry</h5>
        <a href="#" class="color"><em>info@notifyme.ng</em></a>
        <div class="spacer-20px"></div>
        <h5>Technical support</h5>
        <a href="#" class="color"><em>support@notifyme.ng</em></a>
        <div class="spacer-20px"></div>
        <h5>Join us</h5>
        <p> We are always keen to meet new, talented people. If you could work with us, please email your CV and portfolio to </p>
        <a href="#" class="color"><em>careers@notifyme.ng</em></a> </div>
      <!--sidebar ends--> 
      
    </div>
  </div>
@endsection
