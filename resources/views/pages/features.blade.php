@extends('layouts.notifyme')

@section('content')
<div class="container"> 
    
    <!--features starts-->
    <div class="row">
      <div class="span12">
        <h1>What makes <span class="color">Notifyme</span> so special?</h1>
        
        <!--feature list starts-->
        <ul class="features-big">
          
          <!--feature one starts-->
          <li>
            <div class="row">
              <div class="span6">
                <h4>Responsive</h4>
                Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam. </div>
              <div class="span6">
                <ul class="list-icon clearfix visible-desktop">
                  <li> <img src="img/icons/icon1-iphone.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    iphone </li>
                  <li class="plus"><img src="img/plus.png" alt="image" width="14" height="15"></li>
                  <li> <img src="img/icons/icon1-desktop.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    Desktop </li>
                  <li class="plus"><img src="img/plus.png" alt="image" width="14" height="15"></li>
                  <li> <img src="img/icons/icon1-ipad.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    ipad / Tab </li>
                </ul>
                <img src="img/preview/img-window.png" alt="image" class="hidden-desktop"> </div>
            </div>
          </li>
          <!--feature one ends--> 
          
          <!--feature two starts-->
          <li>
            <div class="row">
              <div class="span6">
                <ul class="list-icon clearfix visible-desktop">
                  <li> <img src="img/icons/icon1-android.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    <em class="color">for</em> Android </li>
                  <li class="plus"><img src="img/plus.png" alt="image" width="14" height="15"></li>
                  <li> <img src="img/icons/icon1-iphone.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    <em class="color">for</em> iphone </li>
                  <li class="plus"><img src="img/plus.png" alt="image" width="14" height="15"></li>
                  <li> <img src="img/icons/icon1-blackberry.png" width="120" height="120" alt="icon" class="icon-bg"> <br/>
                    <em class="color">for</em> Blackberry </li>
                </ul>
                <img src="img/preview/tab-slide3.png" alt="image" class="hidden-desktop"> </div>
              <div class="span6">
                <h4>Multi-device Support</h4>
                Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam. </div>
            </div>
          </li>
          <!--feature two ends-->
          
        </ul>
        <!--feature list ends--> 
        
      </div>
    </div>
    <!--features ends--> 
    
    <!--divider here-->
    <div class="spacer-40px"></div>
    
    <!--row starts-->
    <div class="row"> 
      
      <!--column one starts-->
      <div class="span6">
        <h4 class="heading-icon clearfix"> <img src="img/icons/heading-icon-2.png" width="40" height="40" alt="icon" class="icon-small-bg"> Very powerful options built right in</h4>
        <p>Autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.</p>
        
        <!--nested columns / half width list starts-->
        <div class="row">
          <ul class="span3 list-checkmark">
            <li>Easy to use lorem interface</li>
            <li>Cost effective ipsum amet </li>
            <li>Clean layout for lorem</li>
          </ul>
          <ul class="span3 list-checkmark">
            <li>24x7 support on sit amet</li>
            <li>Instant updation sit</li>
            <li>Visually appealing dolor</li>
          </ul>
          <!--nested columns ends--> 
          
        </div>
        <a href="#">Learn more &raquo;</a> </div>
      <!--column one ends--> 
      
      <!--column two / video starts-->
      <div class="span6"><a href="http://vimeo.com/7449107" data-rel="prettyPhoto[gallery1]" title="This is title of vimeo video"><img src="img/video-img.jpg" alt="image"></a> </div>
      <!--column two / video ends--> 
      
    </div>
    <!--row ends--> 
    
    <!--divider here-->
    <div class="spacer-40px"></div>
    
    <!--box starts-->
    <div class="row">
      <div class="span12">
        <div class="dark-box">
          <h1>You are just <span class="color">3</span> steps away</h1>
          <ul class="list-inline">
            <li>1. Install the app</li>
            <li>2. Sync it</li>
            <li>3. Use right away</li>
          </ul>
        </div>
      </div>
    </div>
    <!--box ends--> 
    
</div>
@endsection
