@extends('layouts.canvas')
@section('contents')
  <section id="content">

    <div class="content-wrap">

      <div class="container clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-xs-offset-1">        
                        <div class="margin-bottom--4em">
                          <br>
                          <h2 class="inline">Welcome <span class="capitalize">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span></h2>, 

                          <span class='pull-right'>
                            @if($status=='active')
                              Your 
                              <span class='color--blue bold'> 
                              @if(Auth::user()->trial==1)
                                Trial Period
                              @else Subscription
                              @endif
                              Expires
                              </span>
                              <span class='color--blue bold' data-livestamp="{{$subscription}}" data-toggle="tooltip" title="({{Auth::user()->end_of_subscription}})"></span>  
                              <sub class="expire_date">({{date_format(date_create(Auth::user()->end_of_subscription),"dS-M-Y")}})</sub>                      
                            @else Your <span class='color--red bold'>Subscription Has Expired!</span>
                            @endif
                            <form action="{{url('/payment/callback')}}" method="POST" >
                              {!! csrf_field() !!}
                              <p class="small">Renew Subscription:</p>
                                <select class="small-select" name="months" id="months">
                                  <option selected value="1">1 month</option>
                                  @for($i = 2; $i < 12; $i++)
                                    <option value="{{$i}}">{{$i}} months</option>
                                  @endfor
                                    <option value="12">1 year</option>
                                </select>
                              
                              <button type="button" class="subscribe btn btn-danger" onclick="payWithPaystack()"> Subscribe </button> 
                            </form>
                          </span>
                        </div>
                        @include('common.notify')


                  <div class="tabs tabs-alt tabs-justify clearfix" id="tab-10">
                    <ul class"tab-nav clearfix">
                        <li><a href="#home">Personal Details</a></li>
                        <li><a href="#menu1">Location</a></li>
                        <li><a href="#menu2">Emails</a></li>
                        <li><a href="#menu3">Phone Numbers</a></li>
                    </ul>

                    <div class="tab-container">

                        <div id="home" class="tab-content clearfix">
                          <h3>Personal Details</h3>
                          <p>
                            <form  id="contactform" method="post" action="{{url('user/update')}}">
                              <fieldset>
                                {!! csrf_field() !!}
                                <p>
                                  <label>First Name :</label>
                                  <input name="first_name"  type="text" value="{{Auth::user()->first_name}}">
                                </p>
                                <p>
                                  <label>Last Name :</label>
                                  <input name="last_name"  type="text" value="{{Auth::user()->last_name}}">
                                </p>
                                <p>
                                  <label>Contact Phone Number :</label>
                                  <input name="phone" class="number"  type="phone" value="{{Auth::user()->phone}}">
                                </p>
                                <p>
                                  <label>Contact Email Address :</label>
                                  <input name="email"  class="required email" type="text" value="{{Auth::user()->email}}">
                                </p>
                                <p>
                                  <input type="submit" value="Update" class="submit"/>
                                </p>
                                <div class="clearfix"></div>
                                <div id="result"></div>
                              </fieldset>
                            </form>
                          </p>
                        </div>

                        <div id="menu1" class="tab-content clearfix">
                          <h3>Location</h3>
                          <div class="location_category">                                      
                            @for($i=0; $i<=4; $i++)
                              <div class="hidden">{{$no=$i+1}}</div>
                              <form class="form-inline" action="{{url('location/save')}}" role="form">
                                <div class="form-group">
                                  <div class="span6">                          
                                    <label for="location" class="mylabel">Location {{$i+1}}:</label>
                                    <input type="text" class="form-control" id="location" data-id="{{$no}}" disabled value="@if(isset($location_type[$i])) {{$location_type[$i]}} @endif">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="span6">                          
                                    <label for="street_address" class="mylabel">Street Address:</label>
                                    <input type="text" class="form-control" id="street_address" data-id="{{$i+1}}" value="@if(isset($user['locations'][$i])) {{$user['locations'][$i]['street_address']}} @endif">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="span12">
                                    <label for="city">City:</label>
                                    <input type="text" class="form-control" id="city" data-id="{{$i+1}}" value="@if(isset($user['locations'][$i])) {{$user['locations'][$i]['city']}} @endif">   

                                    <label for="state">State</label>
                                    <select class="country-select" name="state" id="state">
                                      @foreach($states as $state)
                                        <option value="{{$state['name']}}" data-lat="{{$state['latitude']}}" data-log="{{$state['longitude']}}" @if(array_key_exists($i,$user['locations']) && $state['name'] == $user['locations'][$i]['state']) selected @endif >{{$state['name']}}</option>
                                      @endforeach
                                    </select>

                                  </div>
                                </div>
                                <div class="span6">
                                  <button type="button"  class="location-submit btn btn-default" data-id="{{$i+1}}">Save</button>
                                  <span class='btn-success bold saving hidden'>Saving...</span>
                                </div>
                              </form>
                            @endfor
                          </div>
                        </div>

                        <div id="menu2" class="tab-content clearfix">
                          <h3>Emails</h3>
                          <div class="email_category">
                            @for($i=0; $i<=2; $i++)
                            <form class="form-inline" action="{{url('email/save')}}" role="form">
                              <div class="form-group">
                                <label for="email">Email address {{$i+1}}:</label>
                                <input type="email" class="form-control" id="email" value="@if(array_key_exists($i,$user['emails'])) {{$user['emails'][$i]['email']}} @endif" data-id="{{$i+1}}">
                                <button type="button"  class="email-submit btn btn-default">Save</button>
                                <span class='btn-success bold saving hidden'>Saving...</span>
                              </div>
                            </form>
                            <br>
                            @endfor
                          </div>
                        </div>

                        <div id="menu3" class="tab-content clearfix">
                          <h3>Phone Numbers</h3>
                          <div class="phone_category">
                            @for($i=0; $i<=5; $i++)
                            <form class="form-inline" action="{{url('phone/save')}}" role="form">
                              <div class="form-group">
                                <!-- Country Codes -->
                                <select class="country-select" name="phone_country" id="phone_country">
                                  @foreach($country_codes as $country)
                                    <option @if($country['name']=='Nigeria') Selected @endif value="{{$country['altSpellings']['0']}}">{{$country['name']}}</option>                            
                                  @endforeach
                                </select>
                                <label for="phone">Phone Number {{$i+1}}:</label>
                                <input type="number" class="form-control" id="phone" data-id="{{$i+1}}" value="@if(array_key_exists($i,$user['phones'])){{$user['phones'][$i]['phone']}}@endif">                        
                                <button type="button"  class="phone-submit btn btn-default">Save</button>
                                <span class='btn-success bold saving hidden'>Saving...</span>
                              </div>
                            </form>
                            <br>
                            @endfor
                          </div>
                        </div>
                        
                    </div>
                  </div>

                  <h4>Justified Tabs</h4>

            <div class="tabs tabs-alt tabs-justify clearfix" id="tab-10">

              <ul class="tab-nav clearfix">
                <li><a href="#tabs-37"><i class="icon-home2 norightmargin"></i></a></li>
                <li><a href="#tabs-38">Nunc tincidunt</a></li>
                <li><a href="#tabs-39">Proin dolor</a></li>
                <li class="hidden-phone"><a href="#tabs-40">Aenean lacinia</a></li>
              </ul>

              <div class="tab-container">

                <div class="tab-content clearfix" id="tabs-37">
                  Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.
                </div>
                <div class="tab-content clearfix" id="tabs-38">
                  Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
                </div>
                <div class="tab-content clearfix" id="tabs-39">
                  <p>Mauris eleifend est et turpis. Duis id erat. Suspendisse potenti. Aliquam vulputate, pede vel vehicula accumsan, mi neque rutrum erat, eu congue orci lorem eget lorem. Vestibulum non ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce sodales. Quisque eu urna vel enim commodo pellentesque. Praesent eu risus hendrerit ligula tempus pretium. Curabitur lorem enim, pretium nec, feugiat nec, luctus a, lacus.</p>
                  Duis cursus. Maecenas ligula eros, blandit nec, pharetra at, semper at, magna. Nullam ac lacus. Nulla facilisi. Praesent viverra justo vitae neque. Praesent blandit adipiscing velit. Suspendisse potenti. Donec mattis, pede vel pharetra blandit, magna ligula faucibus eros, id euismod lacus dolor eget odio. Nam scelerisque. Donec non libero sed nulla mattis commodo. Ut sagittis. Donec nisi lectus, feugiat porttitor, tempor ac, tempor vitae, pede. Aenean vehicula velit eu tellus interdum rutrum. Maecenas commodo. Pellentesque nec elit. Fusce in lacus. Vivamus a libero vitae lectus hendrerit hendrerit.
                </div>
                <div class="tab-content clearfix" id="tabs-40">
                  Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.
                </div>

              </div>

            </div>


                </div>
            </div>
        </div>
      </div>
    </div>
  </section>
@endsection
