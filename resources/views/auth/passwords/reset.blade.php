@extends('layouts.canvas')

@section('contents')

<!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>My Account</h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('login')}}">Account</a></li>
                    <li class="active">Reset Password</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

<!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">

                        <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Reset Password</div>
                        <div class="acc_content clearfix">
                            <form id="login-form" name="login-form" class="nobottommargin" action="{{ url('/password/reset') }}" method="post">
                                {!! csrf_field() !!}
                                
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="col_full">
                                    <label for="email">E-mail Address:</label>
                                    <input type="text" id="email" name="email" value="{{ $email or old('email') }}" class="form-control" />
                                    <br>
                                    @if ($errors->has('email'))
                                        <span class="help-block text--red">
                                            <strong class='text--red'>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col_full">
                                    <label for="password">Password:</label>
                                    <input type="text" id="password" name="password" class="form-control" />
                                    <br>
                                    @if ($errors->has('password'))
                                        <span class="help-block text--red">
                                            <strong class='text--red'>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col_full">
                                    <label for="password_confirmation">Confirm Password:</label>
                                    <input type="text" id="password" name="password_confirmation" class="form-control" />
                                    <br>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block text--red">
                                            <strong class='text--red'>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>


                                <div class="col_full nobottommargin">
                                    <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" type="submit">
                                        <i class="fa fa-btn fa-envelope"></i> Reset Password</button>
                                </div>
                            </form>
                        </div>                        

                    </div>

                </div>

            </div>

        </section>
<!-- #content end -->

<div class="container hidden">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i>Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
