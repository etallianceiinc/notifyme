@extends('layouts.canvas')

<!-- Main Content -->
@section('contents')

<!-- Page Title
        ============================================= -->
        <section id="page-title">

            <div class="container clearfix">
                <h1>My Account</h1>
                <ol class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('login')}}">Account</a></li>
                    <li class="active">Reset Password</li>
                </ol>
            </div>

        </section><!-- #page-title end -->

<!-- Content
        ============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">

                        <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Reset Password</div>
                        <div class="acc_content clearfix">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form id="login-form" name="login-form" class="nobottommargin" action="{{ url('/password/email') }}" method="post">
                                {!! csrf_field() !!}
                                <div class="col_full">
                                    <label for="email">E-mail Address:</label>
                                    <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control" />
                                    <br>
                                    @if ($errors->has('email'))
                                        <span class="help-block text--red">
                                            <strong class='text--red'>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col_full nobottommargin">
                                    <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" type="submit">
                                        <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link</button>
                                </div>
                            </form>
                        </div>

                        <a href="{{url('login')}}"><i class="acc-open icon-ok-sign"></i>New Signup? Register for an Account</a>
                        

                    </div>

                </div>

            </div>

        </section>
<!-- #content end -->


<div class="container hidden">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-envelope"></i>Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
