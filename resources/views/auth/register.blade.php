@extends('layouts.notifyme')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="padding: 20px 50px;">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Register</h2></div>
                <div class="panel-body">
                    <form class="" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <p>
                            <label class="col-md-4 control-label">First Name</label>
                                <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                        </p>
                        <p>
                            <label class="col-md-4 control-label">Last Name</label>
                                <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                        </p>
                        <p>
                            <label class="col-md-4 control-label">Email Address</label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </p>                        
                        <div>
                        <p>
                            <label class="col-md-4 control-label">Password</label>

                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </p>
                        <p>
                            <label class="col-md-4 control-label">Confirm Password</label>

                                <input type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                        </p>
                        </div>
                        <p class="text-center">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-btn fa-user"></i>Register
                                </button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
