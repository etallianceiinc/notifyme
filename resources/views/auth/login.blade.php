@extends('layouts.canvas')
@section('contents')
<!-- Content============================================= -->
        <section id="content">

            <div class="content-wrap">

                <div class="container clearfix">

                    <div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">

                        <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Login to your Account</div>
                        <div class="acc_content clearfix">
                            {!! csrf_field() !!}
                            <form id="login-form" name="login-form" class="nobottommargin" action="{{ url('/login') }}" method="post">
                                <div class="col_full">
                                    <label for="email">E-mail Address:</label>
                                    <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control" />
                                    <br>
                                    @if ($errors->has('email'))
                                        <span class="help-block text--red">
                                            <strong class='text--red'>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col_full">
                                    <label for="password">Password:</label>
                                    <input type="password" id="password" name="password" class="form-control" />
                                    @if ($errors->has('password'))
                                    <span class="text--red">
                                        <strong class="text--red">{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col_full">
                                    <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                                <div class="col_full nobottommargin">
                                    <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" type="submit">Login</button>
                                    <a href="{{ url('/password/reset') }}" class="fright">Forgot Password?</a>
                                </div>
                            </form>
                        </div>

                        <div class="acctitle"><i class="acc-closed icon-user4"></i><i class="acc-open icon-ok-sign"></i>New Signup? Register for an Account</div>
                        <div class="acc_content clearfix">
                            <form id="register-form" name="register-form" class="nobottommargin" action="{{ url('/register') }}" method="post">
                                {{ csrf_field() }}
                                <div class="col_full">
                                    <label for="first_name">First Name:</label>
                                    <input type="text" id="first_name" name="first_name" value="{{ old('first_name') }}" class="form-control" />

                                    @if ($errors->has('first_name'))
                                    <span class="help-block text--red">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>


                                <div class="col_full">
                                    <label for="last_name">Last Name:</label>
                                    <input type="text" id="last_name" name="last_name" value="{{ old('last_name') }}" class="form-control" />

                                    @if ($errors->has('last_name'))
                                    <span class="help-block text--red">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="col_full">
                                    <label for="email">Email Address:</label>
                                    <input type="text" id="email" name="email" value="{{old('email')}}" class="form-control" />
                                    
                                    @if ($errors->has('email'))
                                    <span class="help-block text--red">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- 
                                <div class="col_full">
                                    <label for="phone">Phone:</label>
                                    <input type="text" id="phone" name="phone" value="" class="form-control" />
                                </div> -->

                                <div class="col_full">
                                    <label for="password">Choose Password:</label>
                                    <input type="password" id="password" name="password" value="" class="form-control" />
                                    @if ($errors->has('password'))
                                    <span class="help-block text--red">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="col_full">
                                    <label for="register-form-repassword">Re-enter Password:</label>
                                    <input type="password" id="register-form-repassword" name="password_confirmation" value="" class="form-control" />
                                </div>

                                <div class="col_full nobottommargin">
                                    <button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" type="submit">Register Now</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>

            </div>

        </section>
<!-- #content end -->
        @stop
