<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('user_id');
            $table->string('location',50);
            $table->string('street_address',100);
            $table->string('lga');
            $table->string('city',40)->nullable();
            $table->string('state',40)->nullable();
            $table->string('country')->default('NG');
            $table->string('longitude');
            $table->string('latitude');
            $table->timestamps();
            //$table->unique('location');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
