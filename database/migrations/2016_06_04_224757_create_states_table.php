<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    /*
        "minLat":4.810874,
      "name":"Abia",
      "capital":"Umuahia",
      "latitude":5.430892099999999,
      "minLong":7.150823,
      "maxLat":6.0191921,
      "longitude":7.524724300000001,
      "maxLong":7.9630091
    */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->string('minLat');
            $table->string('name');
            $table->string('capital');
            $table->string('latitude');
            $table->string('minLong');
            $table->string('maxLat');
            $table->string('longitude');
            $table->string('maxLong');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('states');
    }
}
