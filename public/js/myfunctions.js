
/*Email saving*/
$('.email_category > form > .form-group > .email-submit').click(function() {
	//alert('hi');
	var email=$(this).prev().val();
	var id=$(this).prev().attr('data-id');
	/*var re = /^(([^<>()[]\.,;:s@"]+(.[^<>()[]\.,;:s@"]+)*)|(
		".+"))@(([[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}])|(([a-zA
		-Z-0-9]+.)+[a-zA-Z]{2,}))$/igm;*/
	var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
	if (email == '' || !re.test(email))
	{
		toastr["error"]("Invalid Email Address","Email");		
	    return false;
	}
	//$(this).next().toggleClass('hidden');
	//console.log(email+'id:'+id);
	else{
				toastr["info"]("Saving","Email");			
				toastr.options = {
				  "closeButton": true,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": true,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "1500",
				};

		var xhttp = new XMLHttpRequest();
	        xhttp.onreadystatechange = function() {
	            if (xhttp.readyState == 4 && xhttp.status == 200) {

	            	toastr["success"]('Saved Successfully','Email');
	            	toastr.options = {
					  "closeButton": true,
					  "newestOnTop": true,
					  "positionClass": "toast-top-right",
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					};
	             //console.log(xhttp.responseText);
	             var json= JSON.parse(xhttp.responseText);
	         	}
	         	if (xhttp.status ==500) {
	                    //console.log(xhttp.responseText);
	                   
				}
				if (xhttp.readyState!=4) {	
					
				};
	    	};
	  	xhttp.open("POST", save_email_url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("email="+email+"&id="+id);
	}
});

/*Phone number saving*/
$('.phone_category > form > .form-group > .phone-submit').click(function() {
	//alert('hi');
	var phone=$(this).prev().val();
	var country_field=$(this).parent();//.find('#phone_country');
	//console.log(country_field);
	var country=$(country_field).find('#phone_country').val();
	//var country=$(country_field).prev().val();
	//console.log(country);
	var id=$(this).prev().attr('data-id');
	//$(this).next().toggleClass('hidden');
	//console.log(phone+'id:'+id+' country: '+country);
	if (phone=='' || isNaN(phone) || phone.length<10) {
		toastr["error"]("Invalid Phone  Number","Phone");		
	    return false;
	} 
	else{

				toastr["info"]("Saving","Phone");			
				toastr.options = {
				  "closeButton": true,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": true,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "1500",
				};
		var xhttp = new XMLHttpRequest();
	        xhttp.onreadystatechange = function() {
	            if (xhttp.readyState == 4 && xhttp.status == 200) {
	             //console.log(xhttp.responseText);
	             var json= JSON.parse(xhttp.responseText);
	             toastr["success"]('Saved Successfully','Phone Number');
	            	toastr.options = {
					  "closeButton": true,
					  "newestOnTop": true,
					  "positionClass": "toast-top-right",
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					};
	         	}
	         	if (xhttp.status ==500) {
	                    //console.log(xhttp.responseText);                   
				}
	    	};
	  	xhttp.open("POST", save_phone_url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("phone="+phone+"&id="+id+"&country="+country);
	};
});
/*Location Saving*/
$('.location_category > form > .span6 > .location-submit').click(function() {
	//alert('hi');
	var parent=$(this).parent().parent();
	//console.log(parent);
	/*location*/
	var location=$(parent).find('#location').val();
	//console.log(location);
	/*street_address*/
	var street_address=$(parent).find('#street_address').val();
	var address=encodeURI(street_address.replace(/&amp;/g, "&"));
	/*city*/
	var city=$(parent).find('#city').val();
	/*state*/
	var state=$(parent).find('#state').val();

	/*ID*/
	var id=$(this).attr('data-id');
	if (id=='' || location=='' || street_address=='' || city=='' || state=='' || !isNaN(state) || !isNaN(street_address) || !isNaN(city)) 
	{
		toastr["error"]("Kindly enter full location details","Location");	
		return false;
	} 
	else{

				toastr["info"]("Saving","Location");			
				toastr.options = {
				  "closeButton": true,
				  "positionClass": "toast-top-right",
				  "preventDuplicates": true,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "1500",
				};

		var xhttp = new XMLHttpRequest();
	        xhttp.onreadystatechange = function() {
	            if (xhttp.readyState == 4 && xhttp.status == 200) {
	             //console.log('Response:'+xhttp.responseText);
	             var json= JSON.parse(xhttp.responseText);
	             toastr["success"]('Saved Successfully','Location');
	            	toastr.options = {
					  "closeButton": true,
					  "newestOnTop": true,
					  "positionClass": "toast-top-right",
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					};
	             //console.log(json);
	         	}
	         	if (xhttp.status ==500) {
	                //console.log(xhttp.responseText);                   
				}
	    	};
	  	xhttp.open("POST", save_location_url, true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send('id='+id+'&state='+state+'&city='+city+'&location='+location+'&street_address='+address);
	};
});